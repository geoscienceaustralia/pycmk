from setuptools import setup

setup(
    name="pycmk",
    packages=["pycmk"],
    version="0.31",
    entry_points={
        "console_scripts": [
            "pycmk = pycmk.cli:cli",
            "autocomplete-pycmk = pycmk.autocomplete:autocomplete"
        ]
    },
    install_requires=[
        "boto3",
        "pyneac @ git+https://bitbucket.org/geoscienceaustralia/pyneac",
        "requests",
        "click>=8,<9",
    ],
    description="Python wrapper around CheckMK 1.6.0 API"
)
