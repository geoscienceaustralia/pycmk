# pycmk

This is a Python wrapper around the CheckMK 1.6.0 web API.

The web API reference is available here:
    https://docs.checkmk.com/1.6.0/en/web_api_references.html

Note it's likely not compatible with 2.0.0 (which also introduces
the new REST API which would be preferable to use anyway).

## Installation

Requires Python 3.

Install via pip using:

    pip install git+https://bitbucket.org/geoscienceaustralia/pycmk

To use as a dependency, add this to to your setup.py or requirements.txt:

    pycmk @ git+https://bitbucket.org/geoscienceaustralia/pycmk

## Usage

The pycmk module exposes a variety of ways to interact with the CheckMK
API via scripts or the Python REPL. Refer to `pymck/__init__.py` for the
available methods.

Pycmk will also add a CLI to your path as `pycmk`. Run:

    pycmk --help

for an overview and:

    pycmk <cmd> --help

for help with a specific command.

Bash autocomplete for `pycmk` can be installed by running the command:

    autocomplete-pycmk

If you prefer, you can do this manually by adding:

    eval "$(_PYCMK_COMPLETE=bash_source pycmk)"

to your .bashrc.

## CLI limitations

Adding and editing rules and users can be a complex process as they require
several attributes to be set. This is possible via the commandline by
formatting the attributes as a Python literals in string form, e.g.
`"{'attribute': 'value'}"`. However this is cumbersome, so it's recommended
to do this in code with the pycmk module or via the web interface.

If there is a specific type of interaction you want a command for, e.g.
adding a service to a group, let me know (brenainn.moushall@ga.gov.au).

Alternatively, you can add commands in `pycmk/cli.py`. See the 
`user_enable_notifications` and `user_disable_notifications` for an
example of creating such "shortcut" commands.
