"""Helper to configure autocompletion for the CLI."""
import click
import subprocess
import os


@click.command()
def autocomplete():
    """
    Installs autocompletion for pycmk.
    """
    click.echo(
        "This will activate autocomplete for pycmk by adding:\n "
        'eval "$(_PYCMK_COMPLETE=bash_source pycmk)"\nto your .bashrc'
    )
    click.confirm("Is this okay?", abort=True)
    with open(os.path.expanduser("~/.bashrc"), "r") as f:
        lines = f.readlines()
        for line in lines:
            if 'eval "$(_PYCMK_COMPLETE=bash_source pycmk)"' in line:
                click.echo("Autocomplete already installed. Doing nothing.")
                return
    subprocess.check_call(
        [
            "bash",
            "-c",
            "echo 'eval \"$(_PYCMK_COMPLETE=bash_source pycmk)\"' >> ~/.bashrc",
        ]
    )
    click.echo("Done. Source your bashrc or start a new shell to use.")
