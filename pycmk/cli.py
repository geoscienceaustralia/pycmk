import sys
import json
import ast
from functools import update_wrapper

import click

from pycmk import CheckMKApi, EnvironmentNotFound


def format_result(f):
    @click.pass_context
    def wrapper(ctx, *args, **kwargs):
        try:
            result = ctx.invoke(f, *args, **kwargs)
        except Exception as e:
            click.echo(str(e), err=True)
            sys.exit(1)
        if result is None:
            return
        if not ('-q' in sys.argv or '--quiet' in sys.argv):
            try:
                click.echo(json.dumps(result, indent=2))
            except TypeError:
                click.echo(result)
    return update_wrapper(wrapper, f)

@click.group()
@click.option(
    "-q",
    "--quiet",
    is_flag=True,
    default=False,
    help="Suppress output",
)
@click.option(
    "-h",
    "--host",
    type=str,
    default="localhost",
    help="Hostname of the CheckMK server"
)
@click.option(
    "-p",
    "--port",
    type=int,
    default=5000,
    help="Port the CheckMK server is listening on"
)
@click.option(
    "-s",
    "--site",
    type=str,
    default="prod",
    help="CheckMK site to interact with"
)
@click.option(
    "-u",
    "--username",
    type=str,
    help="CheckMK automation user username"
)
@click.option(
    "-w",
    "--password",
    type=str,
    help="CheckMK automation user password"
)
@click.option(
    "-t",
    "--secret",
    type=str,
    help="AWS Secretsmanager ID of secret containing CheckMK automation username and password."
)
@click.option(
    "-e",
    "--environment",
    type=str,
    help=(
        "The name of a NEAC environment. If a jump host is found for this env, calls "
        "are made to the 'prod' CheckMK site on the jump host - hostname, port, site and "
        "credentials are not required."
    )
)
@click.option(
    "-n",
    "--stack_num",
    type=int,
    help=(
        "Use this stack number if an environment is provided. If not provided, will attempt "
        "to find the active stack."
    )
)
@click.pass_context
def cli(
    ctx,
    quiet=False,
    host='localhost',
    port=5000,
    site='prod',
    username=None,
    password=None,
    secret=None,
    environment=None,
    stack_num=None
):
    """
    Make calls using the CheckMK API.

    If you make changes via the API, you need to activate them before
    they will take effect.

    An automation user must be configured in the CheckMK site.
    You must provide authentication; secret ID takes priority if provided,
    followed by credentials found for the chosen environment, and lastly
    username and password.
    """
    if '--help' not in sys.argv:
        if (username is None and password is None) \
                and (secret is None) \
                and (environment is None):
            click.echo(
                "No authentication. You have to provide an environment, a username and password, "
                "or the name of a secretsmanager secret containing the username and password."
            )
            raise click.Abort()
        cmkapi = CheckMKApi(
            host=host,
            port=port,
            site=site,
            username=username,
            password=password,
            secret_id=secret,
            environment=environment,
            stack_num=stack_num
        )
        ctx.obj = cmkapi


@cli.command()
@click.option(
    "--check",
    is_flag=True,
    default=False,
    help="Check if other changes are pending. If so, changes aren't applied."
)
@click.option(
    "--comment",
    type=str,
    help="Comment to associate with change activation"
)
@click.pass_context
@format_result
def activate(ctx, check=False, comment=None):
    """
    Activate pending changes.
    """
    return ctx.obj.activate_changes(allow_foreign_changes=not check, comment=comment)


@cli.command()
@click.option(
    "--effective",
    is_flag=True,
    default=False,
    help=("If true, only attributes set directly on this host are returned. "
          "If false, default and inherited attributes are included.")
)
@click.argument('hostname')
@click.pass_context
@format_result
def host_get(ctx, hostname, effective=False):
    """
    Get information for a host.
    """
    return ctx.obj.get_host(hostname, effective_attributes=effective)


@cli.command()
@click.pass_context
@format_result
def host_list(ctx):
    """
    List all hosts and their attributes.
    """
    return ctx.obj.get_all_hosts()


@cli.command()
@click.option(
    "--ip",
    type=str,
    help="IP address or network alias of the host"
)
@click.option(
    "--alias",
    type=str,
    help="Alias (human readable name) for the host"
)
@click.argument('hostname')
@click.pass_context
@format_result
def host_add(ctx, hostname, ip=None, alias=None):
    """
    Add a host.
    """
    return ctx.obj.add_host(hostname, ip, alias=alias)


@cli.command()
@click.option(
    "--ip",
    type=str,
    help="New IP address or network alias of the host"
)
@click.option(
    "--alias",
    type=str,
    help="New alias (human readable name) for the host"
)
@click.option(
    "--unset",
    type=str,
    multiple=True,
    help="Attribute to reset to default values (multiple allowed)"
)
@click.argument('hostname')
@click.pass_context
@format_result
def host_edit(ctx, hostname, ip=None, alias=None, unset=None):
    """
    Edit a host.
    """
    return ctx.obj.edit_host(hostname, ipaddress=ip, alias=alias, unset_attributes=unset)


@cli.command()
@click.argument('hostname')
@click.pass_context
@format_result
def host_delete(ctx, hostname):
    """
    Delete a host.
    """
    return ctx.obj.delete_host(hostname)


@cli.command()
@click.option(
    "--mode",
    type=click.Choice(['new', 'remove', 'fixall', 'refresh'], case_sensitive=True),
    default="new",
    help="Mode of discovery"
)
@click.argument('hostname')
@click.pass_context
@format_result
def discover(ctx, hostname, mode="new"):
    """
    Discover services for a host.\n
    Modes:\n
    - new: default, only add new services\n
    - remove: remove services that are no longer available\n
    - fixall: remove services that are no longer avaiable and add new services\n
    - refresh: remove all services and then check for new ones\n
    """
    return ctx.obj.discover_services(hostname, mode=mode)


@cli.command()
@click.option(
    "-g",
    "--grouptype",
    required=True,
    type=click.Choice(['host', 'contact', 'service'], case_sensitive=True),
    help="Type of group"
)
@click.pass_context
@format_result
def group_list(ctx, grouptype):
    """
    List all groups of a type and their attributes.
    """
    return ctx.obj.get_all_groups(grouptype)


@cli.command()
@click.option(
    "-g",
    "--grouptype",
    required=True,
    type=click.Choice(['host', 'contact', 'service'], case_sensitive=True),
    help="Type of group"
)
@click.argument('name')
@click.argument('alias')
@click.pass_context
@format_result
def group_add(ctx, grouptype, name, alias):
    """
    Add a new group.
    """
    return ctx.obj.add_group(grouptype, name, alias)


@cli.command()
@click.option(
    "-g",
    "--grouptype",
    required=True,
    type=click.Choice(['host', 'contact', 'service'], case_sensitive=True),
    help="Type of group"
)
@click.argument('name')
@click.argument('alias')
@click.pass_context
@format_result
def group_edit(ctx, grouptype, name, alias):
    """
    Edit a group's alias.
    """
    return ctx.obj.edit_group(grouptype, name, alias)


@cli.command()
@click.option(
    "-g",
    "--grouptype",
    required=True,
    type=click.Choice(['host', 'contact', 'service'], case_sensitive=True),
    help="Type of group"
)
@click.argument('name')
@click.pass_context
@format_result
def group_delete(ctx, grouptype, name):
    """
    Delete a group.
    """
    return ctx.obj.delete_group(grouptype, name)


@cli.command()
@click.option(
    '--password',
    type=str,
    default='',
    help="Password for the user. Required if you want this user to be able to login to WATO."
)
@click.option(
    '--attributes',
    type=str,
    default='{}',
    help="A string formatted as a Python dictionary containing user attributes."
)
@click.argument('username')
@click.argument('alias')
@click.pass_context
@format_result
def user_add(ctx, username, alias, password='', attributes='{}'):
    """
    Add a new user.

    Because there's a large number of possible user attributes,
    it's recommended to do this via the pycmk module or the web interface.
    """
    return ctx.obj.add_user(username, alias, password, attributes=ast.literal_eval(attributes))


@cli.command()
@click.option(
    '--unset',
    type=str,
    multiple=True,
    help="A list of attributes to reset to default values."
)
@click.option(
    '--set',
    type=str,
    multiple=True,
    help="A list of 'attribute=value' pairs to set."
)
@click.argument('username')
@click.pass_context
@format_result
def user_edit(ctx, username, set={}, unset=[]):
    """
    Edit a user's attributes.

    Because there's a large number of possible user attributes,
    it's recommended to do this via the pycmk module or the web interface.
    """
    if set is not None:
        set = dict(x.split('=') for x in set)
        for k, v in set.items():
            set[k] = ast.literal_eval(v)
    return ctx.obj.edit_user(
        username,
        set_attributes=set if set else {},
        unset_attributes=unset if unset else []
    )


@cli.command()
@click.argument('username')
@click.pass_context
@format_result
def user_get(ctx, username):
    """
    Get a user and their attributes.
    """
    return ctx.obj.get_user(username)


@cli.command()
@click.pass_context
@format_result
def user_list(ctx):
    """
    List all users and their attributes.
    """
    return ctx.obj.get_all_users()


@cli.command()
@click.argument('username')
@click.pass_context
@format_result
def user_delete(ctx, username):
    """
    Delete a user.
    """
    return ctx.obj.delete_user(username)


@cli.command()
@click.pass_context
@format_result
def ruleset_list(ctx):
    """
    List all rulesets.
    """
    return ctx.obj.get_rulesets_info()


@cli.command()
@click.argument('ruleset')
@click.pass_context
@format_result
def rules_get(ctx, ruleset):
    """
    Get a ruleset and its rules.
    """
    return ctx.obj.get_ruleset(ruleset)


@cli.command()
@click.option(
    '--rule',
    multiple=True,
    required=True,
    type=str,
    help="A rule to add formatted as a Python dictionary (multiple allowed)."
)
@click.option(
    '--allow-dupes',
    is_flag=True,
    default=False,
    help="If true, allow adding duplicate rules."
)
@click.option(
    "--append",
    is_flag=True,
    default=True,
    help="If true, append to the existing rules. If false, overwrite the entire ruleset."
)
@click.argument('ruleset')
@click.pass_context
@format_result
def rules_add(ctx, ruleset, rule, allow_dupes=False, append=True):
    """
    Add rules to a ruleset.

    Because the rule structure is complex, it's recommened to do this via
    the pycmk module or via the web interface.

    If --allow-dupes is False (default), then rules with identical
    description are not allowed to be added.
    """
    rules = [ast.literal_eval(x) for x in rule]
    return ctx.obj.add_rules(ruleset, rules, allow_duplication=allow_dupes, append=append)


@cli.command()
@click.option(
    '--rule',
    multiple=True,
    required=True,
    type=str,
    help="Description of a rule to delete from the ruleset."
)
@click.argument('ruleset')
@click.pass_context
@format_result
def rules_delete(ctx, ruleset, rule):
    """
    Delete rules from a ruleset. CheckMK does not have unique IDs for
    rules, so we rely on descriptions being unique. You must supply
    the description exactly as it appears for the rule.
    """
    rules = list(rule)
    return ctx.obj.delete_rules(ruleset, rule_descriptions=rules)


@cli.command()
@click.argument('username')
@click.pass_context
@format_result
def user_disable_notifications(ctx, username):
    """
    Disable notifications for the selected user.
    """
    return ctx.obj.edit_user(username, set_attributes={
        'disable_notifications': {'disable': True}
        }
    )


@cli.command()
@click.argument('username')
@click.pass_context
@format_result
def user_enable_notifications(ctx, username):
    """
    Enable notifications for the selected user.
    """
    return ctx.obj.edit_user(username, unset_attributes=["disable_notifications"])


if __name__ == "__main__":
    cli()
