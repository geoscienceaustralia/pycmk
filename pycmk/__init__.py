"""
Wrapper around the CheckMK API for *version 1.6.0*.
If we update to CheckMK 2.0.0, this will need modifications. In
particular, 2..0.0 introduces the REST-API, intended to cover everything
that can be done via WATO.

The main things the current API (web-api) can't do:
    - global rules
    - notifications rules

These still need to be set via a config file or WATO.

This isn't an exhaustive list of everything this API can do, but targets
what we need to setup our current configuration.

Note using the API requies an automation user to be setup in CheckMK
(with 'admin' permissions if you want to do essentially anything).

You can read the docs for the 1.6 API here:
    https://docs.checkmk.com/1.6.0/en/web_api_references.html

I haven't documented the response structures, so in cases where a
CheckMK response is returned, refer to the official docs for more detail.
"""
from typing import Optional, List
from functools import lru_cache
import json
import subprocess
import ast

from requests.adapters import Retry, HTTPAdapter
import requests

import pyneac


@lru_cache()
def aws_session():
    import boto3
    return boto3.Session()


class CheckMKException(Exception):
    """Raised when a CheckMK API call fails."""


class DuplicateHost(CheckMKException):
    """Raised when a host already exists in CheckMK."""


class HostNotFound(CheckMKException):
    """Raised when a host does not exist in CheckMK."""


class FolderNotFound(CheckMKException):
    """Raised when a folder does not exist in CheckMK."""


class GroupNotFound(CheckMKException):
    """Raised when a group does not exist in CheckMK."""


class UserNotFound(CheckMKException):
    """Raised when a user does not exist in CheckMK."""


class NoChanges(CheckMKException):
    """Raised when no changes are available to activate."""

class EnvironmentNotFound(Exception):
    """Raised when the selected environment isn't available."""


def _load_secret(sid):
    return json.loads(
        aws_session().client("secretsmanager").get_secret_value(SecretId=sid)['SecretString'])


class CheckMKApi:
    """
    Wrapper around Check MK API.

    This requires an automation user (created with automation secret
    and necessary permissions) to exist in the Check MK site.

    Note Check MK API is a little odd in how it takes data.
    Everything is stuffed in a single 'request' argument.
    The request_format parameter can be 'python' or 'json'. If 'python'
    everything in the 'request' variable is evaluated as
    python lietrals (using 'ast.literal_eval').
    If 'json', ditto but deserialised as json.

    Some endpoints only allow the 'python' option, so we stick to that
    all the time.

    It will also read individual args passed as 'key=val' pairs,
    but this doesn't work great as most endpoints require some
    complex structure of nested dicts.

    So ensure any data posted to Check MK API is JSON serialised,
    is sent as 'request=<data>' and be aware that the JSON will be
    evaluated as Python.

    `_make_request` will handle the formatting step.
    Any additional endpoint wrappers just need to pass the data as
    dictionary.

    Parameters
    ----------
    host
        Host that is hosting CheckMK site. Use the default value
        of 'localhost' if you are running this on the CheckMK host
        itself or using SSH to connect remotely (see `environment`
        and `stack_num`). Enter the actual hostname if you are
        directly connecting to a remote CheckMK site.
    port
        Port that the CheckMK site is hosted on.
    site
        Name of the CheckMK site we'll be interacting with.
    username, secret
        Username and automation secret for the automation user.
        If secret_id is provided, it takes priority. At least one
        of `username, secret` or `secret_id` must be provided.
    secret_id
        AWS Secrets Manager ID of a secret containing the
        automation user and secret keyed as 'username' and 'password'
        respectively. Takes precedence over `username, secret`. At least
        one of `username, secret` or `secret_id` must be provided.
    environment
        If provided, will attempt to find a running jump instance
        in the environment. All commands will be through a tunnel
        to the jump instance, allowing remote access. Will also attempt
        to find the jump instance CheckMK sercret, unless username
        and secret or secret_id are provided.
    stack_num
        If this and `environment` are provided, connect to the
        jump instance for this stack. If not provided and `environment`
        is provided, it will default to the active stack. If `environment`
        is not provided, this does nothing.
    """
    def __init__(
        self,
        host: str = 'localhost',
        port: int = 5000,
        site: str = "prod",
        username: Optional[str] = None,
        password: Optional[str] = None,
        secret_id: Optional[str] = None,
        environment: Optional[str] = None,
        stack_num: Optional[int] = None
    ):
        if (username is None and password is None) and secret_id is None and environment is None:
            raise ValueError(
                "Either username and password or secret_id or environment must be provided"
            )

        cmk_creds = None

        if secret_id is not None:
            cmk_creds = _load_secret(secret_id)

        if environment is not None:
            try:
                env = next(e for e in pyneac.get_environments().values() if e.name == environment)
            except StopIteration:
                raise EnvironmentNotFound(f"Environment {environment} not found (do you have the correct AWS_PROFILE set?)")
            if stack_num is None:
                stack = env.active_stack
            else:
                stack = env.stacks[stack_num]
            if not stack:
                raise ValueError(f"No running stack for {environment}")
            self.jump = stack.AutoScalingGroups['jump']
            if not ((username and password) or secret_id):
                cmk_creds = _load_secret(f"{self.jump.Tags['stack_name']}-checkmk-automation")
        else:
            self.jump = None

        self.username = cmk_creds['username'] if cmk_creds else username
        self.password = cmk_creds['password'] if cmk_creds else password
        self.port = port
        if environment is not None:
            if environment == 'prod':
                subdomain = 'jump'
                domain = 'eatws.net'
            else:
                subdomain = f'jump-{environment}'
                domain = 'eatws-nonprod.net'
            if stack_num is not None:
                subdomain = f"{subdomain}{stack_num}"
            self.url = f"http://{subdomain}.{domain}/{site}/check_mk/webapi.py"
        else:
            self.url = f"http://{host}:{port}/{site}/check_mk/webapi.py"
        retry = Retry(total=5, backoff_factor=0.1)
        self._session = requests.Session()
        self._session.mount('http://', HTTPAdapter(max_retries=retry))

    def _make_request(self, action: str, data: dict):
        url = (
            f"{self.url}?action={action}&_username={self.username}&_secret={self.password}"
            f"&request_format=python&output_format=python"
        )
        request_data = {"request": str(data)}
        resp = self._session.post(url, data=request_data)
        obj = ast.literal_eval(resp.text)
        if obj["result_code"] != 0:
            if (
                f"Host {data.get('hostname', '')} already exists in the folder"
                in obj["result"]
            ):
                raise DuplicateHost(obj["result"])
            elif "No such host" in obj["result"]:
                raise HostNotFound(obj["result"])
            elif "Currently there are no changes to activate" in obj["result"]:
                raise NoChanges(obj["result"])
            elif f"Folder {data.get('folder', '')} does not exist" in obj["result"]:
                raise FolderNotFound(obj["result"])
            elif f"Unknown group" in obj["result"]:
                raise GroupNotFound(obj["result"])
            elif f"Unknown user" in obj["result"]:
                raise UserNotFound(obj["result"])
            else:
                raise CheckMKException(obj["result"])
        return obj

    # -- Activate changes -- #
    def activate_changes(
        self,
        allow_foreign_changes: bool = False,
        sites: Optional[List[str]] = None,
        comment: Optional[str] = None,
    ) -> dict:
        """
        Activate pending changes.

        Parameters
        ----------
        allow_foreign_changes
            If True, changes made by other users will also be activated.
            Note if False and changes from users are pending, you will
            not be able to activate changes.
        sites
            A list of site names. Only changes for these
            sites will be activated. If empty (the default), all sites
            will have changes activated.
        comment
            An optional comment to add to the activation.

        Returns
        -------
        dict
            The CheckMK response.
        """
        data = {"allow_foreign_changes": int(allow_foreign_changes)}
        if comment is not None:
            data.update({"comment": comment})
        if sites is not None:
            data.update({"mode": "specific", "sites": sites})
        self._make_request("activate_changes", data)

    # -- Hosts -- #
    def get_host(self, hostname: str, effective_attributes: bool = False) -> dict:
        """
        Get a host's parameters from the CheckMK site.

        Parameters
        ----------
        hostname
            The hostname of the host to get.
        effective_attributes
            If False, only attributes set directly for this host are
            returned. If True, all attributes, including defaults and
            values inherited from the host's directory, will be returned.

        Returns
        -------
        dict
            The CheckMK response.
        """
        data = {"hostname": hostname, "effective_attributes": int(effective_attributes)}
        return self._make_request("get_host", data)

    def add_host(
        self,
        hostname: str,
        ipaddress: Optional[str] = None,
        directory: Optional[str] = None,
        alias: Optional[str] = None,
    ) -> dict:
        """
        Add a host to the Check MK site.
        Note there's a large number of attributes that can be set.
        I've limited this to the ones we currently make use of.

        Parameters
        ----------
        hostname
            The hostname of the host to add.
        ipaddress
            The IP address of the host to add.
        directory
            The directory to add the host to. Default directory is ''
            (root).
        alias
            Optional alias for the host.
        site
            Site to add host to. Default is CheckMK API's site property.

        Returns
        -------
        dict
            The CheckMK response.
        """
        data = {
            "hostname": hostname,
            "folder": directory if directory is not None else "",
        }
        attributes = {}
        if ipaddress is not None:
            attributes.update({"ipaddress": ipaddress})
        if alias is not None:
            attributes.update({"alias": alias})
        data["attributes"] = attributes
        return self._make_request("add_host", data)

    def edit_host(
        self,
        hostname: str,
        ipaddress: Optional[str] = None,
        alias: Optional[str] = None,
        unset_attributes: Optional[List[str]] = None,
    ) -> dict:
        """
        Edit an existing host's attributes.

        Parameters
        ----------
        hostname
            Name of the host to edit.
        ipaddress
            New IP address.
        alias
            New alias.
        unset_attributes
            A list of attribute names to reset to default values.

        Returns
        -------
        dict
            The CheckMK response.
        """
        data = {"hostname": hostname}
        attributes = {}
        if ipaddress is not None:
            attributes.update({"ipaddress": ipaddress})
        if alias is not None:
            attributes.update({"alias": alias})
        if attributes:
            data["attributes"] = attributes
        return self._make_request("edit_host", data)

    def delete_host(self, hostname: str) -> dict:
        """
        Delete a host from ChecMK.

        Parameters
        ----------
        hostname
            Name of the host to delete.

        Returns
        -------
        dict
            The CheckMK response.
        """
        data = {"hostname": hostname}
        return self._make_request("delete_host", data)

    # I've ignored delete_hosts, as we can make multiple calls to
    # delete_host.

    def get_all_hosts(self) -> dict:
        """
        Get all hosts and their attributes.

        Returns
        -------
        dict
            The CheckMK response.
        """
        return self._make_request("get_all_hosts", data={})

    def discover_services(self, hostname: str, mode: str = "new") -> dict:
        """
        Perform service discovery for a host.

        Parameters
        ----------
        hostname
            Name of the host to discover services for.
        mode
            How to handle service discovery.
            - "new": the default setting. Only add new services.
            - "remove": Removes services that are no longer available.
            - "fixall": Removes services that are no longer available
                        and adds new ones.
            - "refresh": Removes all services and adds them again.

        Returns
        -------
        dict
            The CheckMK response.
        """
        # CheckMK won't complain if you give it a non-existing mode.
        if mode not in ("new", "remove", "fixall", "refresh"):
            raise ValueError(
                f"Invalid mode: {mode}, must be one of: 'new', 'remove', 'fixall', 'refresh'"
            )
        data = {"hostname": hostname, "mode": mode}
        return self._make_request("discover_services", data)

    # -- Directories -- #
    def get_folder(self, directory: str) -> dict:
        """
        Get a folder's attributes.

        Parameters
        ----------
        directory
            The directory to get.

        Returns
        -------
        dict
            The CheckMK response.
        """
        data = {"folder": directory}
        return self._make_request("get_folder", data)

    def get_all_folders(self) -> dict:
        """
        Get all folders and their attributes.

        Returns
        -------
        dict
            The CheckMK response.
        """
        return self._make_request("get_all_folders", data={})

    # I've left out adding, editing and deleting directories as we don't
    # need the functionality (everything goes in the root "" directory).

    # -- Groups -- #
    @staticmethod
    def _check_group_type(grouptype: str):
        if grouptype not in ("contact", "service", "host"):
            raise ValueError(
                f"Invalid group type {grouptype}. Must be one of: 'contact', 'service', 'host'"
            )

    def get_all_groups(self, grouptype: str) -> dict:
        """
        Get all groups of a type and their attributes.

        Parameters
        ----------
        grouptype
            One of 'contact', 'service' or 'host'.

        Returns
        -------
        dict
            The CheckMK response.
        """
        self._check_group_type(grouptype)
        return self._make_request(f"get_all_{grouptype}groups", data={})

    def add_group(self, grouptype: str, name: str, alias: str) -> dict:
        """
        Add a group of a type to CheckMK.

        Parameters
        ----------
        grouptype
            One of 'contact', 'service' or 'host'.
        name
            The name of the contact group to add.
        alias
            The alias of the contact group to add.

        Returns
        -------
        dict
            The CheckMK response.
        """
        self._check_group_type(grouptype)
        data = {"groupname": name, "alias": alias}
        return self._make_request(f"add_{grouptype}group", data)

    def edit_group(self, grouptype: str, name: str, alias: str) -> dict:
        """
        Edit a group's alias.

        Parameters
        ----------
        grouptype
            One of 'contact', 'service' or 'host'.
        name
            Name of the contact group to edit.
        alias
            The new alias.

        Returns
        -------
            The CheckMK response.
        """
        self._check_group_type(grouptype)
        data = {"groupname": name, "alias": alias}
        return self._make_request(f"edit_{grouptype}group", data)

    def delete_group(self, grouptype: str, name: str) -> dict:
        """ "
        Delete a group.

        Parameters
        ----------
        grouptype
            One of 'contact', 'service' or 'host'.
        name
            Name of the contact group to delete.

        Returns
        -------
        dict
            The CheckMK response.
        """
        self._check_group_type(grouptype)
        data = {"groupname": name}
        return self._make_request(f"delete_{grouptype}group", data)

    # -- Users -- #
    def add_user(
        self,
        username: str,
        alias: str,
        password: str,
        attributes: Optional[dict] = None,
    ) -> dict:
        """
        Add a user to CheckMK.

        Note this uses plural 'add_users' API endpoint, but we limit
        to adding one user at a time for simplicity.

        Various attributes can be set. Too many to list as parameters
        (CMK docs themselves display a subset). These can be provided as
        a dictionary. Here's an example of a user in our current setup:

        .. example::

            "cmkadmin": {
               "locked": false,
               "roles": [
                 "admin"
               ],
               "contactgroups": [],
               "notification_period": "24X7",
               "num_failed_logins": 0,
               "service_notification_options": "wucrfs",
               "notifications_enabled": true,
               "alias": "cmkadmin",
               "disable_notifications": false,
               "enforce_pw_change": false,
               "last_pw_change": 1644970029,
               "notification_method": "email",
               "serial": 0,
               "password": "$apr1$VrhwsyDP$tLbY8op4P3SsCQ/Mumn3p.",
               "pager": "",
               "email": "jatwc_support@ga.gov.au",
               "host_notification_options": "durfs"
            }

        Parameters
        ----------
        username
            The user's name.
        alias
            The alias (i.e. description) for the user.
        password
            User's password.
        attributes
            An optional dictionary of user attributes to add.

        Returns
        -------
        dict
            The CheckMK response.
        """
        data = {"users": {username: {"alias": alias, "password": password}}}
        if attributes is not None:
            for k, v in attributes.items():
                if isinstance(v, bool):
                    attributes[k] = str(v)
            data["users"][username].update(attributes)
        return self._make_request("add_users", data)

    def edit_user(self, username: str, set_attributes: dict={}, unset_attributes: list=[]) -> dict:
        data = {"users": {username: {
            'set_attributes': set_attributes,
            'unset_attributes': unset_attributes
        }}}
        return self._make_request("edit_users", data)

    def add_automation_user(
        self, username: str, alias: str, secret: str, roles: List[str] = ["admin"]
    ) -> dict:
        """
        Add an automation user to CheckMK.

        Parameters
        ---------
        username
            Name of the user.
        alias
            Descriptive alias for the user.
        secret
            The automation secret.
        roles
            A list of roles to assign. By default, automation users
            have admin privileges.

        Returns
        -------
        dict
            The CheckMK response.
        """
        data = {
            "users": {
                username: {"alias": alias, "automation_secret": secret, "roles": roles}
            }
        }
        return self._make_request("add_users", data)

    def get_all_users(self, username: Optional[str] = None) -> dict:
        """
        Get all users and their attributes.

        Parameters
        ----------
        username
            If provided, only return the user's attributes.
            See `get_user`.

        Returns
        -------
        dict
            The CheckMK response.
        """
        data = {}
        resp = self._make_request("get_all_users", data)
        if username is not None:
            try:
                return resp["result"][username]
            except KeyError as e:
                raise UserNotFound(f"Unknown user: {username}") from e
        else:
            return resp

    def get_user(self, username: str) -> dict:
        """
        Get a user's attributes.

        Parameters
        ----------
        username
            The user's name.

        Returns
        -------
        dict
            The CheckMK response.
        """
        return self.get_all_users(username)

    def delete_user(self, username: str) -> dict:
        """
        Delete a user.

        Parameters
        ----------
        username
            The user's name.

        Returns
        -------
        dict
            The CheckMK response.
        """
        data = {"users": [username]}
        return self._make_request("delete_users", data)

    # -- Rulesets -- #

    def get_ruleset(self, ruleset_name: str) -> dict:
        """
        Get a ruleset from CheckMK.

        Parameters
        ----------
        ruleset_name
            Name of the ruleset

        Returns
        -------
        dict
            The CheckMK response.
        """
        data = {"ruleset_name": ruleset_name}
        return self._make_request("get_ruleset", data)

    def get_rulesets_info(self) -> dict:
        """
        Get information about all rulesets.
        Slightly useless, as it outputs all the default rules and
        there is a lot of them.

        Returns
        -------
        dict
            The CheckMK response.
        """
        return self._make_request("get_rulesets_info", data={})

    def add_rules(
        self,
        ruleset_name: str,
        rules: List[dict],
        allow_duplication: bool = False,
        overwrite: bool = False,
        append: bool = True,
    ) -> dict:
        """
        Add rules to a ruleset.

        Note that CheckMK sets the entire ruleset at once, overwriting
        existing rules. Added rules will be appended to the current
        ruleset, unless you turn off the `append` flag.

        CheckMK happily accepts duplicate rules. We could test for
        equality, but the slightest change in the config could lead
        to unintentionally duplicating a rule.

        To deal with this, *we rely on descriptions being unique within
        a ruleset*.

        Every rule added must have its own unique description.

        Parameters
        ----------
        ruleset_name
            Name of the ruleset to add rules to.
        rules
            A list of rules to add. Each rule is a dictionary.
        allow_duplication
            If True, we won't check for unqiue descriptions and allow
            duplicate rules to be added.
        append
            If True, added rules will be appended to the current ruleset.
            Otherwise, ruleset will be overwritten. When not appending,
            we don't bother to check for conflicts with existing rules.
        """
        for rule in rules:
            if "options" not in rule or "description" not in rule["options"]:
                raise ValueError(
                    "Every rule must have a description. "
                    "This is keyed as rule['options']['description']."
                )

        # Yes we can do this without looping, but I want to include the conflicting rules
        # in the error.
        our_descriptions = [rule["options"]["description"] for rule in rules]
        flagged = set()
        for desc in our_descriptions:
            if our_descriptions.count(desc) > 1:
                flagged.add(f"'{desc}'")
        if flagged:
            raise ValueError(
                f"Attempting to add duplicated rules: {', '.join(flagged)}"
            )

        current_rules = self.get_ruleset(ruleset_name)["result"]["ruleset"].get("", [])
        if not allow_duplication and append:
            conflicts = []
            for current_rule in current_rules:
                for desc in our_descriptions:
                    if current_rule.get("options", {}).get("description") == desc:
                        conflicts.append(
                            f"Rule '{current_rule['options']['description']}' "
                            f"conflicts with new rule '{desc}'"
                        )
            if conflicts and overwrite:
                self.delete_rules(ruleset_name, conflicts)
            elif conflicts and not overwrite:
                raise ValueError(
                    f"Conflicts with existing rules: {', '.join(conflicts)}"
                )

        new_rules = rules if not append else rules + current_rules
        data = {"ruleset_name": ruleset_name, "ruleset": {"": new_rules}}
        return self._make_request("set_ruleset", data)

    def delete_rules(self, ruleset_name: str, rule_descriptions: List[str]) -> dict:
        """
        Delete rules from a ruleset.

        Parameters
        ----------
        ruleset_name
            Name of the ruleset to delete rules from.
        rule_descriptions
            A list of descriptions of rules to delete.

        Returns
        -------
        dict
            The CheckMK response.
        """
        current_rules = self.get_ruleset(ruleset_name)["result"]["ruleset"].get("", [])
        new_rules = []
        for rule in current_rules:
            if rule.get("options", {}).get("description") not in rule_descriptions:
                new_rules.append(rule)
        data = {"ruleset_name": ruleset_name, "ruleset": {"": new_rules}}
        return self._make_request("set_ruleset", data)

    def add_custom_check(
        self, hostname: str, service_name: str, description: str, comment: str = ""
    ):
        """
        This is a shotcut for adding a custom service and is primarily
        for use with Detcap.
        """
        check = [
                    {
                        "condition": {"host_name": [hostname]},
                        "value": {"service_description": service_name},
                        "options": {"comment": comment, "description": description},
                    }
                ]
        self.add_rules("custom_checks", check, overwrite=True)
        
        # We only want to alert on critical and recovery.
        notification_options = [
                    {
                         "condition": {
                             "service_description": [{"$regex": f"{service_name}$"}],
                             "host_name": [hostname]
                         },
                         "value": "c,r",
                         "options": {"comment": comment, "description": description}
                    }
                ]
        self.add_rules("extra_service_conf:notification_options", notification_options, overwrite=True)

        # Disable flapping for alerts
        flapping_options = [
                    {
                         "condition": {
                             "service_description": [{"$regex": f"{service_name}$"}],
                             "host_name": [hostname]
                         },
                         "value": "0",
                         "options": {"comment": comment, "description": description}
                    }
                ]
        self.add_rules("extra_service_conf:flap_detection_enabled", flapping_options, overwrite=True)
 
    def remove_custom_check(self, description: str):
        """
        This is a shortcut for removing a custom service and is primarily
        for use with Detcap.
        """
        self.delete_rules("custom_checks", [description])
        self.delete_rules("extra_service_conf:notification_options", [description]) 
        self.delete_rules("extra_service_conf:flap_detection_enabled", [description]) 

    def push_check(self, hostname: str, service_name: str, code: int, message: str,
            additional_output: Optional[str] = None
    ):
        """
        This can only be done if an environment is provided, allowing us
        to SSH to the jump host and run the push command.

        If an environment wasn't provded, it attempts to run the command
        locally (assumes the command is being run on the jump host
        itself).
        """
        result = f'PROCESS_SERVICE_CHECK_RESULT;{hostname};{service_name};{code};{message}'
        if additional_output is not None:
            result += '\\n{}'.format(additional_output)
        cmd = (
            f'echo "[$(date +%s)] '
            f'{result}"'
            f" | sudo tee /opt/omd/sites/prod/tmp/run/nagios.cmd > /dev/null"
        )

        if self.jump is not None:
            self.jump.connect().run(cmd)
        else:
            subprocess.check_call(cmd, shell=True)
